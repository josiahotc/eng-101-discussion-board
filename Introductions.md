#My Post

Hey everyone, my name is Josiah Roberts. I am starting at OTC this fall to pursue computer science. In a situation somewhat like Jeremy Womack's, I've been working in the industry for eight years without any degree. However, now that I have the time and money, I am getting a degree to broaden my career and academic opportunities.

As far as hobbies, interests, and general quirks, I am very interested in philosophy, having studied the subject for several years at a school that is unfortunately not accredited. Other interests include physics, technology, and not dying of malaria.

#My Replies

##To JEREMY WOMACK

> My name is Jeremy Womack.  I was lucky to start a career 17 years ago that required a bachelor�s degree.  I have flown under the radar without a degree the entire time.  Last year I applied for a corporate position within our company, I received the promotion and agreed to obtain my bachelors in business at the company�s expense.  I chose to start at OTC for several reasons, convenience, cost and availability of online classes.  My hobbies include traveling, camping, hunting and fishing.  My phobia is spiders.

Hey Jeremy, it sounds like we have had pretty similar career experiences - I started at age 15 and am also lacking a bachelor's degree. My field is software engineering, but I am curious: what kind of work do you do?